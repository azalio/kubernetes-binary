#!/bin/bash

ETCD_VER="v3.4.16"
PKG_ITERATION=0

# choose either URL
GOOGLE_URL=https://storage.googleapis.com/etcd
# GITHUB_URL=https://github.com/etcd-io/etcd/releases/download
DOWNLOAD_URL=${GOOGLE_URL}

rm -f "/tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz"
rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test

curl -L "${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz" -o "/tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz"
tar xzvf "/tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz" -C /tmp/etcd-download-test --strip-components=1
rm -f "/tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz"

fpm -s dir -n "etcd" \
-v "${ETCD_VER}" \
-t rpm --rpm-os linux \
-a x86_64 \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov, <azalio@azalio.net>" \
--description "ETCD binaries" \
--iteration "${PKG_ITERATION}" \
--rpm-group "etcd" \
/tmp/etcd-download-test/etcd=/usr/bin/etcd \
/tmp/etcd-download-test/etcdctl=/usr/bin/etcdctl \
# -p /tmp/etcd-download-test \

fpm -s dir -n "etcd-alt-${ETCD_VER}" \
-v "1" \
-t rpm --rpm-os linux \
-a x86_64 \
--license "Apache Software License 2.0" \
--maintainer "Roman Kuzmitskii, <@damexoid>" \
--description "ETCD binaries" \
--iteration "${PKG_ITERATION}" \
--rpm-group "etcd" \
/tmp/etcd-download-test/etcd=/usr/bin/etcd-${ETCD_VER} \
/tmp/etcd-download-test/etcdctl=/usr/bin/etcdctl-${ETCD_VER}
