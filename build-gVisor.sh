#!/bin/bash

GVISOR_CONTAINERD_SHIM_VERSION="0.0.4"
GVISOR_VERSION="20200323.0"
PKG_ITERATION="1"
INSTALL_PATH="/usr/bin"

# choose either URL
GOOGLE_URL="https://storage.googleapis.com/gvisor/releases/release"
GITHUB_URL="https://github.com/google/gvisor-containerd-shim/releases/download"

rm -f "./${INSTALL_PATH}"
rm -f "./*.rpm"
mkdir -p "./${INSTALL_PATH}"

wget -q "${GOOGLE_URL}"/"${GVISOR_VERSION}"/runsc
wget -q "${GITHUB_URL}"/"v${GVISOR_CONTAINERD_SHIM_VERSION}"/containerd-shim-runsc-v1.linux-amd64
wget -q "${GITHUB_URL}"/"v${GVISOR_CONTAINERD_SHIM_VERSION}"/gvisor-containerd-shim.linux-amd64

mv runsc                                ./"${INSTALL_PATH}"/runsc
mv containerd-shim-runsc-v1.linux-amd64 ./"${INSTALL_PATH}"/containerd-shim-runsc-v1
mv gvisor-containerd-shim.linux-amd64   ./"${INSTALL_PATH}"/gvisor-containerd-shim

chmod +x ./"${INSTALL_PATH}"/{runsc,containerd-shim-runsc-v1,gvisor-containerd-shim}

fpm -s dir \
-t rpm --rpm-os linux \
-n "gvisor" \
-v ${GVISOR_VERSION} \
-a x86_64 \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov, <azalio@azalio.net>" \
--description "gVisor runtime" \
--iteration ${PKG_ITERATION} \
./${INSTALL_PATH}/runsc

fpm -s dir \
-t rpm --rpm-os linux \
-n "gvisor-containerd-shim" \
-v ${GVISOR_CONTAINERD_SHIM_VERSION} \
-a x86_64 \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov, <azalio@azalio.net>" \
--description "gvisor-containerd-shim is a containerd shim for gVisor" \
--iteration ${PKG_ITERATION} \
./${INSTALL_PATH}/{containerd-shim-runsc-v1,gvisor-containerd-shim}
