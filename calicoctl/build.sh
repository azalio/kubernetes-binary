#!/bin/bash

VER="v3.13.5"
PKG_ITERATION=1

URL=https://github.com/projectcalico/calicoctl/releases/download/${VER}/calicoctl-linux-amd64
mkdir -p /tmp/${VER}

curl -L "${URL}" -o "/tmp/${VER}/calicoctl"
chmod +x "/tmp/${VER}/calicoctl"

fpm -s dir -n "calicoctl" \
-v "${VER}" \
-t rpm --rpm-os linux \
-a x86_64 \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov, <azalio@azalio.net>" \
--description "calicoctl binaries" \
--iteration "${PKG_ITERATION}" \
--rpm-group "calicoctl" \
/tmp/${VER}/calicoctl=/usr/local/bin/calicoctl

rm -rf /tmp/${VER}
