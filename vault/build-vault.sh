#!/bin/bash

VAULT_VER="1.4.0"
PKG_ITERATION=0

URL=https://releases.hashicorp.com/vault/${VAULT_VER}/vault_${VAULT_VER}_linux_amd64.zip
mkdir -p /tmp/vault

curl -L "${URL}" -o "/tmp/vault/vault_${VAULT_VER}_linux_amd64.zip"
pushd /tmp/vault || exit 1
unzip "vault_${VAULT_VER}_linux_amd64.zip"
popd || exit 1

fpm -s dir -n "vault" \
-v "${VAULT_VER}" \
-t rpm --rpm-os linux \
-a x86_64 \
--license "Apache Software License 2.0" \
--maintainer "Mikhail Petrov, <azalio@azalio.net>" \
--description "vault binaries" \
--iteration "${PKG_ITERATION}" \
--rpm-group "vault" \
/tmp/vault/vault=/usr/local/bin/vault

rm -rf /tmp/vault
