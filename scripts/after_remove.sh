#!/usr/bin/env bash

GROUPNAME="kube"
USERNAME="kube"
# Skip in case of package upgrade
if [ $1 -ne 1 ] ; then
    getent passwd ${USERNAME} >/dev/null && userdel ${USERNAME}
    getent group ${GROUPNAME} >/dev/null && groupdel ${GROUPNAME}
fi
